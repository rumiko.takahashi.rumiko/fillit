/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/01 21:34:17 by vbudnik           #+#    #+#             */
/*   Updated: 2017/11/29 18:11:22 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int	*ft_intmap(int const *s, int (*f)(int))
{
	int	*res;
	int		size;
	int		i;

	i = 0;
	size = 0;
	if (s && f)
	{
		while (s[size] != -1)
			size++;
		res = (int *)malloc((size) + 1);
		if (res == 0)
			return (0);
		while (i < size)
		{
			res[i] = f(s[i]);
			i++;
		}
		res[size] = -1;
		return (res);
	}
	return (0);
}








